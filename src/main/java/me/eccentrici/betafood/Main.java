package me.eccentrici.betafood;

import me.eccentrici.betafood.commands.bfa;
import me.eccentrici.betafood.events.OnEat;
import me.eccentrici.betafood.events.OnJoin;
import me.eccentrici.betafood.events.OnRespawn;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

public final class Main extends JavaPlugin {
    public static Plugin instance = null;

    FileConfiguration config = getConfig();
    @Override
    public void onEnable() {
        instance = this;
        getServer().getPluginManager().registerEvents(new OnJoin(this), this);
        getServer().getPluginManager().registerEvents(new OnEat(this), this);
        getServer().getPluginManager().registerEvents(new OnRespawn(this), this);
        getCommand("bfa").setExecutor(new bfa(this));
        config.options().copyDefaults(true);
        saveDefaultConfig();
        String version = this.getDescription().getVersion();
        Bukkit.getLogger().info(String.format("[BetterFoodAdvanced %s] Enabled!", version));
        Bukkit.getLogger().info("Thank you for using BetaFoodAdvanced.");

    }

    @Override
    public void onDisable() {
        instance = null;
    }



}
