package me.eccentrici.betafood.commands;

import me.eccentrici.betafood.Main;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class bfa implements CommandExecutor {
    private final Main main;

    public bfa(Main main) {
        this.main = main;
    }
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (command.getName().equalsIgnoreCase("bfa")){
                String version = this.main.getDescription().getVersion();

                player.sendMessage(String.format("§a[BFA %s] Check for updates at: §nhttps://www.spigotmc.org/resources/beta-food-advanced.99622/", version));
                return true;
            }
            return true;
        }

        return true;
    }
}
