package me.eccentrici.betafood.events;


import me.eccentrici.betafood.Main;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerRespawnEvent;


public class OnRespawn implements Listener {
    private final Main main;
    public OnRespawn(Main main) {
        this.main = main;
    }

    @EventHandler
    public void onPlayerRespawn(PlayerRespawnEvent event) {
        Player player = event.getPlayer();
        Bukkit.getScheduler().runTaskLater(main, () -> {
            player.setFoodLevel(8); // 4 food bars
        }, 1L);


    }
}
