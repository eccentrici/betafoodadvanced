package me.eccentrici.betafood.events;

import me.eccentrici.betafood.Main;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import java.util.HashMap;
import java.util.Locale;


public class OnEat implements Listener {

    HashMap<String, Integer> lm = new HashMap<>();
    private final Main main;

    public OnEat(Main main) {
        this.main = main;
        // Manually defining them in a HashMap.
        lm.put("beetroot", 1);
        lm.put("dried_kelp", 1);
        lm.put("potato", 1);
        lm.put("pufferfish", 1);
        lm.put("tropical_fish", 1);
        lm.put("cookie", 2);
        lm.put("glow_berries", 2);
        lm.put("melon_slice", 2);
        lm.put("poisonous_potato", 2);
        lm.put("chicken", 2);
        lm.put("cod", 2);
        lm.put("mutton", 2);
        lm.put("salmon", 2);
        lm.put("spider_eye", 2);
        lm.put("sweet_berries", 2);
        lm.put("carrot", 3);
        lm.put("beef", 3);
        lm.put("porkchop", 3);
        lm.put("rabbit", 3);
        lm.put("apple", 4);
        lm.put("chorus_fruit", 4);
        lm.put("enchanted_golden_apple", 4);
        lm.put("golden_apple", 4);
        lm.put("rotten_flesh", 4);
        lm.put("baked_potato", 5);
        lm.put("bread", 5);
        lm.put("cooked_cod", 5);
        lm.put("cooked_rabbit", 5);
        lm.put("beetroot_soup", 6);
        lm.put("cooked_chicken", 6);
        lm.put("cooked_mutton", 6);
        lm.put("cooked_salmon", 6);
        lm.put("golden_carrot", 6);
        lm.put("honey_bottle", 6);
        lm.put("mushroom_stew", 6);
        lm.put("suspicious_stew", 6);
        lm.put("cooked_porkchop", 8);
        lm.put("pumpkin_pie", 8);
        lm.put("cooked_beef", 8);
        lm.put("rabbit_stew", 10);
    }
    /*
        Important for stopping hunger from moving by default.
     */
    @EventHandler (priority = EventPriority.HIGH)
    public void onFoodLevelChange (FoodLevelChangeEvent event) {
        if (event.getEntityType () != EntityType.PLAYER) return;
        event.setCancelled (true);
    }
    /*
        Triggered anytime the player Eats
     */
    @EventHandler
    public void onPlayerEat(PlayerItemConsumeEvent event) {
        Player player = event.getPlayer();
        double maxHealth = player.getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue(); // Getting the player's max health, in case they have any buffs.
        Bukkit.getScheduler().runTaskLater(main, () -> {
            player.setFoodLevel(8); // Setting the player food level back to its default, which should be 8 anyway.
            //player.sendMessage(event.getItem().getType().name().toLowerCase(Locale.ROOT));
            // Checking if the overall value is lower than the Maximum Value, otherwise just set to the max to prevent any errors.
            if ((player.getHealth() + lm.get(event.getItem().getType().name().toLowerCase(Locale.ROOT)) >= maxHealth)){
                player.setHealth(maxHealth);
            }
            else{
                player.setHealth(player.getHealth() + (lm.get(event.getItem().getType().name().toLowerCase())));
            }
        }, 1);

    }

    // Material.CAKE is included within PlayerItemConsumeEvent.
    @EventHandler
    public void onCakeEat(PlayerInteractEvent event){
        double maxHealth = event.getPlayer().getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue();

        if ((event.getClickedBlock() != null) && (event.getAction() == Action.RIGHT_CLICK_BLOCK)){
            if (event.getClickedBlock().getType() == Material.CAKE){
                Bukkit.getScheduler().runTaskLater(main, () -> {
                    event.getPlayer().setFoodLevel(8);
                    if ((event.getPlayer().getHealth() + 2 >= maxHealth)){
                        event.getPlayer().setHealth(maxHealth);
                    } else {
                        event.getPlayer().setHealth(event.getPlayer().getHealth() + 2);
                    }
                }, 2);
            }
        }
    }
}

