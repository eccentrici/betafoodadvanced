package me.eccentrici.betafood.events;

import me.eccentrici.betafood.Main;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerResourcePackStatusEvent;

public class OnJoin implements Listener {
    private final Main main;

    public OnJoin(Main main) {
        this.main = main;
    }

    // Triggered upon a player joining
    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        player.setFoodLevel(8); // 4 food bars
        if (main.getConfig().getBoolean("resourcepackEnable")) {
            Bukkit.getScheduler().runTaskLater(main, () -> {
                player.setResourcePack("https://download.mc-packs.net/pack/da145dfcb67e3407ad15e6e2d5f2c0a130d05eab.zip");
            }, 1);
        }

    }

    // If user rejects pack.
    @EventHandler
    public void onResourcepackStatusEvent(PlayerResourcePackStatusEvent event) {
        if (main.getConfig().getBoolean("resourcepackForce")) {
            if(event.getStatus() == PlayerResourcePackStatusEvent.Status.DECLINED) {
                String kickmessage = main.getConfig().getString("resourcepackKickMessage");
                event.getPlayer().kickPlayer(kickmessage);

            }
        }

    }

}
