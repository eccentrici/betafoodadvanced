### Beta Food Advanced v1.3.0
Beta Food Advanced is a Minecraft Bukkit/Spigot/Paper plugin which reintroduces the beta food mechanics that were removed in Beta 1.8; Prior to this update, using food would directly heals health. BFA tries to bring this back - or atleast imitate it! Get it here: https://www.spigotmc.org/resources/beta-food-advanced.99622/

### Benefits
- Only eat when you take damage.
- Customizable config.
- Custom resource pack that can be enforced or not.
- Relive your nostalgic memories
- You can call yourself an OG (Cool guy basically, get all the women).

### To Do
- Enforce an configurable Server resource pack which removes the food icons all together. (DONE)

### Simple explanation
BetaFoodAdvanced removes the usage of the "Hunger bar" replacing it with a system that uses the health bar. On a player eating, the "nutrition value" that is gained is added directly to health.
### The boring explanation
BFA is a fairly simple concept; we stop the player from being able to increase thier FoodLevel; this is done like this:

```java
@EventHandler
public void onPlayerEat(PlayerItemConsumeEvent event) {
        Player player = event.getPlayer();
        double maxHealth = player.getAttribute(Attribute.GENERIC_MAX_HEALTH).getDefaultValue();
        // Keep Player Food intact.
        Bukkit.getScheduler().runTaskLater(main, () -> {
        player.setFoodLevel(8); // 4 food bars
        .....
```
A similar function is integrated for other events such as; PlayerRespawnEvent, PlayerJoinEvent, PlayerInteractEvent (used for cake). `setFoodLevel(8)` is used to set the FoodLevel to 8 (4 whole icons); the viable FoodLevel - low enough that you do not regenerate, yet high enough so you can sprint. We get the Nutrition Value from a HashMap; I don't use NMS, because we all know it's **Shit**.
